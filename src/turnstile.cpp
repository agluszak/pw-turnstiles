#include "turnstile.h"
#include <iostream>
#include <sstream>
#include <thread>

using pw_turnstile_::POINTER_PROTECTORS_COUNT;
using pw_turnstile_::Pool;
using pw_turnstile_::Turnstile;
using std::array;
using std::hash;
using std::lock_guard;
using std::mutex;
using std::unique_lock;

Turnstile* get_empty_turnstile() {
  static struct EmptyTurnstile {
  } e;
  static Turnstile* empty_turnstile = reinterpret_cast<Turnstile*>(&e);
  return empty_turnstile;
}

mutex& get_pointer_protector(Mutex* m) {
  static hash<Mutex*> ptr_hash;
  static array<mutex, POINTER_PROTECTORS_COUNT> pointer_protectors;
  size_t index(ptr_hash(m) % POINTER_PROTECTORS_COUNT);
  return pointer_protectors[index];
}

Pool& get_pool() {
  static Pool pool;
  return pool;
}

Mutex::Mutex() : turnstile(nullptr) {}

void Mutex::lock() {
  unique_lock<mutex> pointer_lock(get_pointer_protector(this));
  if (turnstile == nullptr) {
    turnstile = get_empty_turnstile();
  } else {
    if (turnstile == get_empty_turnstile()) {
      turnstile = get_pool().get_turnstile();
    }
    turnstile->count++;
    pointer_lock.unlock();
    unique_lock<mutex> sleep_lock(turnstile->mutex);
    turnstile->cv.wait(sleep_lock, [&] { return turnstile->ready; });
    pointer_lock.lock();
    turnstile->ready = false;
    turnstile->count--;

    if (turnstile->count == 0) {
      get_pool().return_turnstile(turnstile);
      turnstile = get_empty_turnstile();
    }
    pointer_lock.unlock();
  }
}

void Mutex::unlock() {
  assert(turnstile != nullptr);
  unique_lock<mutex> pointer_lock(get_pointer_protector(this));
  if (turnstile == get_empty_turnstile()) {
    turnstile = nullptr;
  } else {
    unique_lock<mutex> sleep_lock(turnstile->mutex);
    turnstile->ready = true;
    turnstile->cv.notify_one();
  }
}

Pool::Pool() : all_count(INITIAL_TURNSTILES_COUNT) {
  for (size_t i = 0; i < INITIAL_TURNSTILES_COUNT; ++i) {
    free_queue.push(new Turnstile);
  }
}

Pool::~Pool() {
  while (!free_queue.empty()) {
    Turnstile* t = free_queue.front();
    free_queue.pop();
    delete t;
  }
}

Turnstile* Pool::get_turnstile() {
  lock_guard<mutex> lock(pool_protector);
  if (free_queue.empty()) {
    size_t need_to_allocate = all_count;
    for (size_t i = 0; i < need_to_allocate; ++i) {
      free_queue.push(new Turnstile);
      all_count++;
    }
  }
  Turnstile* ret = free_queue.front();
  free_queue.pop();
  return ret;
}

void Pool::return_turnstile(Turnstile* turnstile) {
  assert(turnstile->count == 0 && "Tried to return non-empty turnstile!");
  lock_guard<mutex> lock(pool_protector);
  free_queue.push(turnstile);
  //     More than 75% of turnstiles are not used
  if (all_count > INITIAL_TURNSTILES_COUNT && free_queue.size() * 4 > all_count * 3) {
    size_t need_to_deallocate = all_count / 2;
    for (size_t i = 0; i < need_to_deallocate; ++i) {
      Turnstile* t = free_queue.front();
      free_queue.pop();
      delete t;
      all_count--;
    }
  }
}
