#include "../turnstile.h"

#include <iostream>
#include <map>
#include <mutex>
#include <stdexcept>
#include <thread>
#include <vector>
using namespace std;


int counter = 0;
Mutex m;

void add() {
    for (int i = 0; i < 1000; ++i) {
//        this_thread::sleep_for(std::chrono::milliseconds(10));
        m.lock();

        int x = counter;

        x++;
        counter = x;
        m.unlock();
    }
}

int main() {

    try {

        std::thread first (add);
        std::thread second (add);
        std::thread third (add);
        first.join();
        second.join();
        third.join();
        cout << counter;
    } catch (std::exception &e) {
        std::cout << "Exception: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
