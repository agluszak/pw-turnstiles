#ifndef SRC_TURNSTILE_H_
#define SRC_TURNSTILE_H_

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <mutex>
#include <optional>
#include <queue>
#include <type_traits>

namespace pw_turnstile_ {
static constexpr size_t POINTER_PROTECTORS_COUNT = 257;

static constexpr size_t INITIAL_TURNSTILES_COUNT = 16;

struct Turnstile {
  Turnstile() = default;
  Turnstile(const Turnstile&) = delete;

  size_t count{0};
  bool ready{false};
  std::mutex mutex;
  std::condition_variable cv;
};

class Pool {
  std::mutex pool_protector;
  std::queue<Turnstile*> free_queue;
  size_t all_count;

 public:
  Pool();
  ~Pool();

  Turnstile* get_turnstile();

  void return_turnstile(Turnstile* turnstile);
};

}  // namespace pw_turnstile_

class Mutex {
  pw_turnstile_::Turnstile* turnstile;

 public:
  Mutex();

  Mutex(const Mutex&) = delete;

  void lock();    // NOLINT
  void unlock();  // NOLINT
};

#endif  // SRC_TURNSTILE_H_
